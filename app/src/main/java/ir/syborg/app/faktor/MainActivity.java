package ir.syborg.app.faktor;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener{

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Spinner spinner1=(Spinner) findViewById(R.id.spinner1);
    Spinner spinner2=(Spinner) findViewById(R.id.spinner2);
    Spinner spinner3=(Spinner) findViewById(R.id.spinner3);


    spinner1.setOnItemSelectedListener(this);
    ArrayAdapter<CharSequence> adapter= ArrayAdapter.createFromResource(this, R.array.stuff,android.R.layout.simple_spinner_item);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinner1.setAdapter(adapter);

  }
  @Override
  public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
    if(position!= 0){
      Toast.makeText(getApplicationContext(), arg0.getItemAtPosition(position).toString(),Toast.LENGTH_SHORT).show();

    }
  }
  @Override
  public void onNothingSelected(AdapterView<?> arg0) {

  }
}
